(in-package #:common-lisp-user)

(defpackage #:solr
  (:use #:cl)
  (:export #:start-solr-logging
		   #:stop-solr-logging
           #:*solr-options*
           #:*solr-core*
           #:with-solr-options
           #:with-solr-core
		   #:with-solr-connection
		   #:connect-solr
           #:solr-delete-all
           #:solr-post
           #:solr-get
           #:solr-add
           #:solr-update
           #:solr-select
		   #:solr-commit
		   #:solr-optimize
		   #:solr-status

           #:solr-escape
           #:format-solr-query
           #:*query-escape*))
