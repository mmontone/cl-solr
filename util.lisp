(in-package :solr)

(defun lower-camel-case (string)
  (let ((parts (split-sequence:split-sequence #\- string)))
    (apply #'concatenate 'string
                 (cons (string-downcase (first parts))
                       (mapcar 
                        (lambda (string)
                          (string-capitalize (string-downcase string)))
                        (rest parts))))))

(defun upper-camel-case (string)
  (let ((parts (split-sequence:split-sequence #\- string)))
    (apply #'concatenate 'string
           (mapcar 
            (lambda (string)
              (string-capitalize (string-downcase string)))
            parts))))

(defun xpath-evaluate (xpath context &optional unordered-p)
  (let ((xpath:*navigator* (cxml-xmls:make-xpath-navigator)))
    (xpath:evaluate xpath context unordered-p)))

(defun make-keyword (string)
  (intern (string-upcase string) :keyword))

(defun join-string-list (string-list)
    "Concatenates a list of strings
and puts spaces between the elements."
    (format nil "~{~A~^-~}" string-list))

(defun camel-case-to-keyword (string)
  (let ((words nil)
        (word ""))
    (loop for char across string
       do
         (if (and (>= (char-code char) (char-code #\A))
                  (<= (char-code char) (char-code #\Z)))
             (progn
               (push word words)
               (setf word (format nil "~A" char)))
             (setf word (format nil "~A~A" word char)))
         finally (push word words))
    (make-keyword (join-string-list (reverse words)))))

(defparameter +solr-datetime-format+
  ;; 2008-11-18T02:32:00.586931+01:00
  '((:year 4) #\- (:month 2) #\- (:day 2) #\T
    (:hour 2) #\: (:min 2) #\: (:sec 2) #\Z))

(defun format-universal-time (universal-time)
  (local-time:format-timestring nil (local-time:universal-to-timestamp universal-time)
                                :format +solr-datetime-format+))