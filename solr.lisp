(in-package :solr)

(defvar *solr-options* (list :commit "false"))
(defvar *solr-core* nil)

(defparameter +default-solr-url+ "http://localhost:8983")

(defvar *solr-connection* +default-solr-url+)

(log5:defcategory solr)

(defun start-solr-logging ()
  (log5:start-sender 'solr
                     (log5:stream-sender :location *standard-output*)
                     :category-spec '(solr log5:info+)
                     :output-spec '(log5:time log5:category log5:message)))

(defun stop-solr-logging ()
  (log5:stop-sender 'solr))

(defun connect-solr (&optional (url +default-solr-url+))
  (setf *solr-connection* url))

(defun get-solr-core (&optional (core *solr-core*))
  (or core (error "No solr core selected. Wrap this with WITH-SOLR-CORE")))

(defun call-with-solr-connection (solr-connection function)
  (let ((*solr-connection* solr-connection))
    (funcall function)))

(defmacro with-solr-connection ((&optional (url '+default-solr-url+))
                                &body body)
  `(call-with-solr-connection ,url
                              (lambda () ,@body)))

(defun call-with-solr-options (options function)
  (let ((*solr-options* options))
    (funcall function)))

(defmacro with-solr-options (options &body body)
  `(call-with-solr-options (list ,@options)
                           (lambda () ,@body)))

(defun call-with-solr-core (solr-core function)
  (let ((*solr-core* solr-core))
    (funcall function)))

(defmacro with-solr-core (solr-core &body body)
  `(call-with-solr-core ,solr-core
                        (lambda () ,@body)))

(defun solr-escape (query)
  "Escape a Lucene query.

See: https://lucene.apache.org/core/2_9_4/queryparsersyntax.html#Escaping%20Special%20Characters"
  (let ((regex
		 '(:alternation #\+ #\- "&&" "||" #\! #\( #\)
		   #\{ #\} #\^ #\" #\~ #\* #\? #\: #\\)))
	(ppcre:regex-replace-all
	 (ppcre:create-scanner regex)
	 query
	 (lambda (match)
	   (concatenate 'string "\\" match))
	 :simple-calls t)))

(defun solr-delete-all (&rest args)
  (log5:log-for (solr log5:info) "Deleting all documents")
  (apply #'solr-post :update "<delete><query>id:[* TO *]</query></delete>" args))

(defmethod solr-request :before (uri &rest args)
  (log5:log-for (solr log5:info) "~A ~A (Content type: ~A)" (getf args :method) uri (getf args :content-type))
  (when (getf args :content)
    (log5:log-for (solr) "Content: ~A" (babel:octets-to-string (getf args :content)))))

;; (defmethod solr-request :around (uri &rest args)
;;    (let ((response (apply #'call-next-method uri args)))
;;      (log5:log-for (solr) "Result: ~A" (cxml:parse (babel:octets-to-string response) (cxml-xmls:make-xmls-builder)))
;;      response))

(defmethod solr-request (uri &rest args)
  (apply #'drakma:http-request uri (append args (list :preserve-uri t))))

(define-condition solr-error (simple-error)
  ())

(defun solr-error (msg)
  (error 'solr-error :format-control "~A" :format-arguments (list msg)))

(defmethod solr-post (action content &rest options
					  &key (core *solr-core*) &allow-other-keys)
  (let ((options (append options *solr-options*)))
	(let ((url (format nil "~A/solr/~A/~A"
					   *solr-connection*
					   (get-solr-core core)
					   (string-downcase (symbol-name action)))))
	  (when options
		(setf url (format nil "~A?~{~A~^&~}"
						  url
						  (mapcar (lambda (key-value)
									(destructuring-bind (key . value) key-value
									  (format nil "~A=~A" (lower-camel-case (symbol-name key)) value)))
								  (alexandria:plist-alist options)))))
	  (multiple-value-bind (response status)
		  (solr-request url
						:method :post
						:content-type "text/xml"
						:content
						(babel:string-to-octets content))
		(let ((response (cxml:parse (babel:octets-to-string response) (cxml-xmls:make-xmls-builder))))
		  (unless (= status 200)
			(let ((xpath (xpath-evaluate "//str[@name='msg']" response)))
			  (solr-error
			   (destructuring-bind (node-name attrs value) (xpath:first-node xpath)
				 (declare (ignore node-name attrs))
				 value))))
		  response
		  )))))

(defun solr-get (action &rest args &key (core *solr-core*) &allow-other-keys)
  (let ((url (format nil "~A/solr/~A/~A"
                     *solr-connection*
					 (get-solr-core core)
                     (string-downcase (symbol-name action)))))
    (when args
      (setf url (format nil "~A?~{~A~^&~}"
                        url
                        (mapcar (lambda (key-value)
                                  (destructuring-bind (key . value) key-value
                                    (format nil "~A=~A" (lower-camel-case (symbol-name key))
                                            (drakma:url-encode value :utf-8))))
                                (alexandria:plist-alist args)))))
	(multiple-value-bind (response status)
        (solr-request url
                      :method :get
                      :content-type "text/xml")
	  (let ((response (cxml:parse (babel:octets-to-string response)
								  (cxml-xmls:make-xmls-builder))))
		
        (unless (= status 200)
          (let ((xpath (xpath-evaluate "//str[@name='msg']" response)))
            (solr-error
             (destructuring-bind (node-name attrs value) (xpath:first-node xpath)
               (declare (ignore node-name attrs))
               value))))
        response
        ))))

(defun solr-select (query &rest args)
  (let ((response
         (apply #'solr-get :select :q query args)))
    (let ((qtime (parse-integer (third (xpath:first-node (xpath-evaluate "//int[@name='QTime']" response)))))
          (num-found (parse-integer (cxml-xmls::xmls-attribute-value (xpath:first-node (xpath-evaluate "//result/@numFound" response)))))
          (results (xpath:all-nodes (xpath-evaluate "//doc" response))))
      (values (mapcar #'parse-solr-doc results) num-found qtime))))

(defmethod solr-update ((document cons) &rest args)
  (apply #'solr-update (xmls:toxml document) args))

(defmethod solr-update ((document string) &rest args)
  (apply #'solr-post :update document args))

(defun solr-add (document &rest args &key (document-type :plist) &allow-other-keys)
  (ecase document-type
    (:plist (apply #'%solr-add-document
				   document
				   #'alexandria:plist-alist
				   args))
	(:alist (apply #'%solr-add-document
				   document
				   #'identity
				   args))))

(defun solr-commit (&rest args &key (core *solr-core*))
  (log5:log-for (solr log5:info) "Commit")
  (solr-request (format nil "~A/solr/~A/update"
						*solr-connection*
						(get-solr-core core))
				:method :post
				:content-type "text/xml"
				:content
				(babel:string-to-octets "<commit/>")))

(defun solr-optimize (&rest args &key (core *solr-core*))
  (log5:log-for (solr log5:info) "Optimize")
  (solr-request (format nil "~A/solr/~A/update"
						*solr-connection*
						(get-solr-core core))
				:method :post
				:content-type "text/xml"
				:content
				(babel:string-to-octets "<optimize/>")))

(defun %solr-add-document (document fields-reader &rest args)
  (let ((doc
         (with-output-to-string (str)
           (cxml:with-xml-output (cxml:make-character-stream-sink str :indentation nil :omit-xml-declaration-p t)
             (cxml::with-element "add"
			   (when (getf args :commit-within)
				 (cxml:attribute "commitWithin" (getf args :commit-within)))
               (cxml::with-element "doc"
                 (loop for (field . value) in (funcall fields-reader document)
                    do
                      (progn
                        (cxml::with-element "field"
                          (cxml::attribute "name" (lower-camel-case (symbol-name field)))
                          (cxml::text (format nil "~A" value)))))))))))
    (apply #'solr-update doc args)))

(defun parse-solr-value (value type)
  (case type
    (:int (parse-integer value))
    (:long (parse-integer value))
    (t value)))

(defun parse-solr-doc-to-plist (doc)
  (let ((fields (cddr doc)))
    (loop for field in fields
       appending
         (destructuring-bind (type name &optional value) field
           (let ((name (second (first name))))
             (list (camel-case-to-keyword name)
                   (parse-solr-value value (make-keyword type))))))))

(defun parse-solr-doc-to-alist (doc)
  (let ((fields (cddr doc)))
    (loop for field in fields
       collect
         (destructuring-bind (type name &optional value) field
           (let ((name (second (first name))))
             (cons (camel-case-to-keyword name)
                   (parse-solr-value value (make-keyword type))))))))

(defun parse-solr-doc (doc &optional (output-type :plist))
  (case output-type
    (:plist (parse-solr-doc-to-plist doc))
    (:alist (parse-solr-doc-to-alist doc))))

(defun solr-status (&key (core *solr-core*))
  (let ((uri (if core
				 (format nil "~A/solr/admin/cores?action=STATUS&core=~A"
						 *solr-connection*
						 core)
				 (format nil "~A/solr/admin/cores?action=STATUS"
						 *solr-connection*))))
	(multiple-value-bind (response status)
		(drakma:http-request uri
							 :content-type "text/xml"
							 :preserve-uri t)
	  (unless (= status 200)
		(solr-error "Cannot connect to Solr"))
	  (babel:octets-to-string response))))
