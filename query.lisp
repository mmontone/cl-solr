(in-package :solr)

;; A simple Solr query formatter

(defvar *query-escape* t)

(defun format-solr-query (query stream)
  (format-solr-term query stream))

(defun format-solr-term (term stream)
  (flet ((final-string (s)
           (if *query-escape*
               (solr-escape s)
               s)))
    (cond
      ((stringp term)
       (if (find #\space term)
           (progn (write-char #\" stream)
                  (princ (final-string term) stream)
                  (write-char #\" stream))
           (princ (final-string term) stream)))
      ((and (listp term)
            (not (keywordp (first term))))
       (format-solr-term (cons :seq term) stream))
      ((and (listp term)
            (keywordp (first term)))
       (format-solr-term-type (first term) term stream))
      (t (princ (solr-escape term) stream)))))

(defgeneric format-solr-term-type (term-type term stream)

  (:method ((term-type (eql :escape)) term stream)
    (let ((*query-escape* t))
      (format-solr-term (second term) stream)))
  (:method ((term-type (eql :esc)) term stream)
    (format-solr-term-type :escape term stream))

  (:method ((term-type (eql :noesc)) term stream)
    (let ((*query-escape* nil))
      (format-solr-term (second term) stream)))

  (:method ((term-type (eql :raw)) term stream)
    (princ (second term) stream))

  (:method ((term-type (eql :seq)) term stream)
    (destructuring-bind (_ &rest terms) term
      (loop
         :for term :in terms
         :for next := (cdr terms) :then (cdr next)
         :do (format-solr-term term stream)
         :when next
         :do (write-char #\space stream))))

  (:method ((term-type (eql :phrase)) term stream)
    (destructuring-bind (_ &rest words) term
      (format stream "\"~{~A~^ ~}\"" words)))

  (:method ((term-type (eql :=)) term stream)
    "field: value"
    (destructuring-bind (_ field value) term
      (write-string (string field) stream)
      (write-string ": " stream)
      (format-solr-term value stream)))

  (:method ((term-type (eql :wildcard)) term stream)
    "value*"
    (destructuring-bind (_ value &optional (position :end)) term
      (ecase position
        (:start
         (write-string "*" stream)
         (format-solr-term value stream))
        (:end
         (format-solr-term value stream)
         (write-string "*" stream)))))

  (:method ((term-type (eql :*)) term stream)
    (format-solr-term-type :wildcard term stream))

  (:method ((term-type (eql :fuzzy)) term stream)
    "value~similarity"
    (destructuring-bind  (_ value &optional similarity) term
      (format-solr-term value stream)
      (write-string "~" stream)
      (when similarity
        (princ similarity stream))))

  (:method ((term-type (eql :~)) term stream)
    (format-solr-term-type :fuzzy term stream))

  (:method ((term-type (eql :proximal)) term stream)
    (destructuring-bind (_ phrase proximity) term
      (if (listp phrase)
          (format stream "\"~{~A~^ ~}\"" phrase)
          (format stream "\"~A\"" phrase))
      (write-string "~" stream)
      (princ proximity stream)))

  (:method ((term-type (eql :range)) term stream)
    (destructuring-bind (_ (from from-type) (to to-type)) term
      (ecase from-type
        (:include (write-char #\[ stream))
        (:exclude (write-char #\{ stream)))
      (princ from stream)
      (write-string " TO " stream)
      (princ to stream)
      (ecase to-type
        (:include (write-char #\] stream))
        (:exclude (write-char #\} stream)))))

  (:method ((term-type (eql :boost)) term stream)
    (destructuring-bind (_ term factor) term
      (format-solr-term term stream)
      (write-char #\^ stream)
      (princ factor stream)))

  (:method ((term-type (eql :^)) term stream)
    (format-solr-term-type :boost term stream))

  (:method ((term-type (eql :and)) term stream)
    (destructuring-bind (_ &rest terms) term
      (loop
         :for term :in terms
         :for next := (cdr terms) :then (cdr next)
         :do (format-solr-term term stream)
         :when next
         :do (write-string " AND " stream))))

  (:method ((term-type (eql :&&)) term stream)
    (format-solr-term-type :and term stream))

  (:method ((term-type (eql :or)) term stream)
    (destructuring-bind (_ &rest terms) term
      (loop
         :for term :in terms
         :for next := (cdr terms) :then (cdr next)
         :do (format-solr-term term stream)
         :when next
         :do (write-string " OR " stream))))

  (:method ((term-type (eql :||)) term stream)
    (format-solr-term-type :or term stream))

  (:method ((term-type (eql :required)) term stream)
    (write-char #\+ stream)
    (format-solr-term (second term) stream))
  (:method ((term-type (eql :+)) term stream)
    (format-solr-term-type :required term stream))

  (:method ((term-type (eql :not)) term stream)
    (write-string "NOT " stream)
    (format-solr-term (second term) stream))
  (:method ((term-type (eql :!)) term stream)
    (format-solr-term-type :not term stream))

  (:method ((term-type (eql :prohibit)) term stream)
    (write-char #\- stream)
    (format-solr-term (second term) stream))
  (:method ((term-type (eql :-)) term stream)
    (format-solr-term-type :prohibit term))

  (:method ((term-type (eql :group)) term stream)
    (destructuring-bind (_ &rest terms) term
      (write-char #\( stream)
      (format-solr-term (list :seq terms) stream)
      (write-char #\) stream)))

  )

#+test
(progn

  (defun fq (q)
    (with-output-to-string (s)
      (format-solr-query q s)))

  (fq '(:= "lala" (:~ (:* "lolo") 0.2)))

  (fq '(:= "title" "super duper* AND yes"))

  (fq '(:phrase "lala" "lolo"))

  (fq '(:proximal ("lala" "lolo") 10))

  (fq '(:range (22 :include) (28 :exclude)))

  (fq '(:range ("Aida" :exclude) ("Carmen" :exclude)))

  (fq '(:boost "Aida" 22))

  (fq '(:fuzzy (:* "aaa")))

  (fq '(:and (:= "lala" (:~ (:* (:esc "lolo")) 0.2))
        (:= "title" "super duper")))

  (fq '(:noesc "lala*"))

  (fq '(:or (:= "lala" (:~ (:* (:esc "lolo")) 0.2))
        (:= "title" "super duper")))

  (fq '(:or (:required "jakarta") "lucene"))

  (fq '(:seq "lala" (:required "lolo")))
  (fq '("lala" (:required "lolo")))

  (fq '(:= "title" ("jakarta apache" (:not "Apache Lucene"))))

  (fq '(:= "title" ("jakarta" (:prohibit "Apache Lucene"))))

  (fq '(:= "title" (:group (:required "return") (:required "pink panther")))))
