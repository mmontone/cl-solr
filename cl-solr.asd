(defsystem #:cl-solr
  :name "cl-solr"
  :description "Common Lisp Solr Client"
  :licence "GNU General Public Licence 3.0"
  :depends-on (:drakma
			   :cxml
			   :log5
			   :stefil
			   :split-sequence
			   :xpath
			   :xmls
			   :local-time
			   :cl-ppcre)
  :serial t
  :components ((:file "package")
               (:file "util")
               (:file "query")
               (:file "solr")))
